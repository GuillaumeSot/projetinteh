PRAGMA foreign_keys=OFF;
BEGIN TRANSACTION;
INSERT INTO contact VALUES(1,'pcpascher','XXX 73100 Chambéry','pcpascher@gmail.com','0606060606');
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('contact',1);

INSERT INTO produit VALUES(1,'1er ORDI','10 000 €','Ordinateur Mc powerbook collector de 1994.','../images/ordi.jpg','"mac powerbook"');
INSERT INTO produit VALUES(2,'2eme ORDI','15 €','Ordinateur portable double lecteur de disquette.','../images/pcannees80.jpg','"double disquette"');
INSERT INTO produit VALUES(3,'3eme ORDI','2,5 €','Ordinateur en carton.','../images/pccarton.jpg','"pc en carton"');
INSERT INTO produit VALUES(4,'4eme ORDI','159 €','Ordi victor vintage.','../images/pcportable.jpg','"pc vintage"');
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('produit',4);

INSERT INTO equipe VALUES(1,'Notre équipe !','gigi.jpeg','Guillaume','Développeur IT','Lorem ipsum dolor sit amet, consectetur adipiscing elit. Duis nec efficitur libero, iaculis faucibus nibh. Duis volutpat leo non mi molestie volutpat nec id odio. Duis ');
INSERT INTO equipe VALUES(2,'Notre équipe !','robin.jpg','Robin','Leadership','accumsan ac. Nulla sed nisi nunc. Sed sed dui ex. Aenean lobortis dui eu ante tincidunt, quis porta ex iaculis.\n\nNam imperdiet metus urna, id tempus purus pellentesque sit amet. ');
INSERT INTO equipe VALUES(3,'Notre équipe !','corentin.jpg','Corentin','Fondateur','Vestibulum blandit nisi at felis volutpat, et vehicula augue dapibus. Orci varius natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Mauris sollicitudin ipsum id risus ultrices ');
INSERT INTO equipe VALUES(4,'Notre équipe !','loic.jpg','Loïc','Chef de projet','interdum semper pharetra. Quisque sapien ipsum, varius at laoreet id, hendrerit sit amet felis. Ut vitae interdum sapien, sed maximus ex. Maecenas ');
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('equipe',4);

INSERT INTO actu VALUES(1,"Nouvelle gamme Nimbusware 2000","","");
INSERT INTO actu VALUES(2,"Vers le gaming et au-delà.","Bientôt dans nos magasins.","wallpaper-pcgamer.jpeg");
INSERT INTO actu VALUES(3,"Super Soldes de noël","Accéder à la boutique >","Christmas-Sale-image-design-4.jpeg");
INSERT INTO actu VALUES(4,"Grand jeux de l'hiver","90% de réduction sur tous nos produits","coffrets-cadeaux-fond-blanc_1205-2760.jpeg");
INSERT INTO actu VALUES(5,"Oeuvrons tous pour un monde meilleur !","Un produit acheté , les quatres autres offerts, nos agents viendront chez vous pour sortir vos poubelles et faire le ménage.","sigleslabelsbiook.jpeg");
INSERT INTO actu VALUES(6,"","Venez participer à notre grand jeux de l'hiver et tentez de repartir avec plus de 10 000€ de cadeaux ! ","2logo-biodegradable.jpeg");
INSERT INTO actu VALUES(7,"","Pcpaschère s'engage cette année à vendre des ordinateurs entièrement biodégradables et sans huile de palme. ","");
INSERT INTO actu VALUES(8,"","Durant le mois vert , pour un ordinateur acheté 10 arbres planté . Faites vite et sauvez la planète en achetant chez nous ! ","");
DELETE FROM sqlite_sequence;
INSERT INTO sqlite_sequence VALUES('actu',8);

COMMIT;
