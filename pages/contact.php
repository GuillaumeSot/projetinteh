<?php

$fichierDb='../pcpascher.db';
try {
      $bd = new PDO('sqlite:'.$fichierDb);
    }
catch(PDOException $e){
      echo $e->getMessage();
     }
    // Affichage info pcpascher
$info = $bd->prepare("SELECT * FROM contact");
$info->execute();
$contacts = $info->fetchAll(PDO::FETCH_OBJ);



    // Envoie Formulaire
if(isset($_POST['bouton'])){
    // Validation du formulaire, le message d'erreur est en JS
    if ($_REQUEST['nom'] === '' or $_REQUEST['prenom'] === '' or $_REQUEST['email'] === '' or $_REQUEST['telephone'] === ''){
     
    }
    else{
        // Lancement de la requête
        try {
            $sql="INSERT INTO sendcontact (nom, prenom, email, telephone) VALUES (:nom, :prenom, :email, :telephone)";
            $stmt = $bd->prepare($sql);
            $stmt->bindParam(':nom', $_REQUEST['nom']);
            $stmt->bindParam(':prenom', $_REQUEST['prenom']);
            $stmt->bindParam(':email', $_REQUEST['email']);
            $stmt->bindParam(':telephone', $_REQUEST['telephone']);

            $stmt->execute();
            }

        catch(PDOException $e){
            echo $e->getMessage();
            }
    }
}
?>


<!DOCTYPE html>
<html>
<head>
    <title>contact</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Ceci est la page contact du site pcpascher !"/>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>

    <link rel="stylesheet" href="../css/style.css">
    <script type="text/javascript" src="../javascript/contact.js"></script>
</head>
    <body id="body">
<?php
include "header.php";
?>

<a href="#" class="btn btn-secondary btn-lg active m-5 b-dark" id="dark" role="button" aria-pressed="true" onclick="dark()" ondblclick="white()" >Mode Dark</a>


        <div class="container" style="margin-top:4%">
         <h1 class="text-center" style="font-size:4em">Contact</h1>
            <div class="contact">
             <h2 class="text-center"><strong>Nous Contacter</strong></h2>
                 <p class="text-center">
                    <?php foreach($contacts as $contact): ?>
                        Nom: <strong> <?=$contact->nom ?> </strong></br>
                        Telephone: <strong> <?=$contact->telephone?> </strong></br>
                        E-mail : <strong> <?=$contact->email ?> </strong></br>
                        Adresse : <strong> <?=$contact->adresse?></strong>
                    <?php endforeach ?>
                 </p>
            </div>
         </div>
         <div class="container">
         <h2 class="text-center" style="margin-top: 6%"><strong>Vos Informations</strong></h2>
            <div class="formContact">

             <form name="formContact" onsubmit="Validation()" method="post" action="contact.php">
                    <div class="col-md-3 col-sm-4 col-6 formulaire">
                        <label for="idNom" class="form-label">Nom</label>
                        <input type="text" class="form-control" id="idNom" name="nom">
                    </div>
                    <div class="col-md-3 col-sm-4 col-6 formulaire">
                        <label for="idPrenom" class="form-label">Prenom</label>
                        <input type="text" class="form-control" id="idPrenom" name="prenom">
                    </div>
                    <div class="col-md-3 col-sm-4 col-6 formulaire">
                        <label for="idEmail" class="form-label">Email</label>
                        <input type="email" class="form-control" id="idEmail" name="email">
                    </div>
                    <div class="col-md-3 col-sm-4 col-6 formulaire">
                        <label for="idTelephone" class="form-label">Telephone</label>
                        <input type="tel" class="form-control" id="idTelephone" name="telephone">
                    </div>
                    <div class="col-md-3 col-sm-4 col-6 formulaire">
                        <button class="btn btn-primary" type="submit" name="bouton">Enregistrer</button>
                    </div>
            </form>
            </div>
        </div>


<?php
include "footer.php";
?>
    </body>
</html>