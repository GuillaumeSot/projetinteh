<!DOCTYPE html>
<html lang="en">
<head>
    
    <title>nos produits</title>
    <meta name="description" content="Ceci est la page produit du site pcpascher !"/>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
        integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo" crossorigin="anonymous">
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous">
    </script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous">
    </script>
    
    <link rel="stylesheet" type="text/css" href="../css/style.css" />
    <script type="text/javascript" src="../javascript/produit.js"></script>
    
    <style>
        .element{
           filter:brightness(100%);
        }
        
        .elem{
            filter:brightness(10%);
        }
       
     
     
    </style>
   
   
   
    </head>
<body  id="body">
<?php

include "header.php";
?>


<?php
    try {
        $pdo = new PDO("sqlite:../pcpascher.db");
    } catch (PDOException $e) {
        echo $e->getMessage();
    }
    $prod = $pdo->prepare("SELECT * FROM produit /*ORDER BY id ASC*/");
    $prod->execute();
    $info = $prod->fetchAll(PDO::FETCH_OBJ);
?>
    
    <a href="#" class="btn btn-secondary btn-lg active m-5 b-dark" id="dark" role="button" aria-pressed="true" onclick="dark()" ondblclick="white()" >Mode Dark</a>

    <h1 class="text-center display-2 " style="margin-top:50px;">Nos Produits</h1>

    
    <div class="container " >

        <div class="row d-flex justify-content-around " style="margin-top:100px;">

               
        
        <?php foreach( $info as $infos): ?>
        
            
            
            <div id= "block1" class="col-md-6">
                   
                <div class="card-body" >
                            
                            <div class= "elem" id= <?=$infos->id?> (<?=$infos->id?>) onmouseover=darkimage(<?=$infos->id?>)   onmouseout =brightimage(<?=$infos->id?>)>
                            <img  src= "<?=$infos->photo?>"  class="img-fluid " alt="<?=$infos->altimage?>" >
                            </div>
                            
                            <h1> <?=$infos->nom?> </h1>  <h2> <?=$infos->prix?> </h2>
                            <p><?=$infos->description?></p>
                </div>
                
            </div> 

       
                <?php endforeach ?>
        </div>
        
    </div>
    

        <?php
        include "footer.php";
        ?>
    </body>
</html>